function sentenceToUpperCase(sentence) {
  const sentenceSplit = sentence.split(" ");
  if (sentenceSplit.length === 1) {
    return sentence.toUpperCase();
  } else {
    let final = sentenceSplit.map((ele, index) =>
      index % 2 === 0 ? ele : ele.toUpperCase()
    );
    final = final.join(" ");
    return final;
  }
}
